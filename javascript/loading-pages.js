fetch('pages/task-list.html')
    .then(response => response.text())
    .then(htmlPage => {
        document.querySelector('.main-page__task-list').innerHTML = htmlPage;
    })
    .catch(error => {
        console.error('Error loading the task list:', error);
    });

fetch('pages/header.html')
    .then(response => response.text())
    .then(htmlPage => {
        document.querySelector('.main-page__header').innerHTML = htmlPage;

        const menuItems = document.querySelectorAll('.header__menu input[type="radio"]');

        menuItems.forEach((item) => {
            item.addEventListener('change', function(radio) {
                if (radio.target.checked) {
                    loadPageContent(radio.target.id);
                }
            });
        });

        const checkedMenuItem = document.querySelector('.header__menu input[type="radio"]:checked');
        loadPageContent(checkedMenuItem.id);

    })
    .catch(error => {
        console.error('Error loading the header:', error);
    });

function loadPageContent(pageId) {
    const pageMap = {
        'home-page': 'dashboard.html',
        'student-list-page': 'student-list.html',
        'task-list-page': 'task.html'
    };

    const pageFileName = pageMap[pageId];

    fetch(`pages/${pageFileName}`)
        .then(response => response.text())
        .then(htmlContent => {
            document.querySelector('.main-page__content').innerHTML = htmlContent;
        })
        .catch(error => {
            console.error('Error loading the page:', error);
        });
}





