export default class Student {
    constructor(group = "ПЗ-28", name = "Viktor Boem", gender = "Male", birthday = "12.06.2005", status = "active") {
        this.group = group;
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.status = status;
    }
}
