import PopupManager from "../modules/popup-manager.js";

export default class TableManager {
    constructor(table, addRowButton, studentData) {
        this.table = table;
        this.tbody = this.table.querySelector('tbody');
        this.addRowButton = addRowButton;
        this.studentData = studentData;

        this.checkBoxManager = new CheckBoxManager(this.table, this.tbody);
        this.popupManager = new PopupManager();
    }

    init() {
        this.addRowButton.addEventListener('click', () => this.addRow(this.studentData));
        this.checkBoxManager.initCheckedOrUnchecked();
    }

    addRow(rowData) {
        const newRow = this.createRow(rowData);

        this.tbody.appendChild(newRow);
    }

    createRow(rowData) {
        const newRow = document.createElement('tr');

        newRow.innerHTML =
        `<td>
            <label class="students-list__check">
                <input type="checkbox"/>
                <span class="students-list__checkmark"></span>
            </label>
        </td>
        <td>${rowData.group}</td>
        <td>${rowData.name}</td>
        <td>${rowData.gender}</td>
        <td>${rowData.birthday}</td>
        <td>${rowData.status}</td>
        <td>
            <button class="student-list__button student-list__button--delete"><img src="../images/delete.svg" alt="delete"/></button>
            <button class="student-list__button student-list__button--edit"><img src="../images/edit.svg" alt="edit"/></button>
        </td>`;

        this.attachDeleteHandler(newRow);
        return newRow;
    }

    attachDeleteHandler(rowElement) {
        const deleteButton = rowElement.querySelector('.student-list__button--delete');

        deleteButton.addEventListener('click', () => {

            this.popupManager.confirm().then((result) => {
                if(result) {
                    rowElement.style.animation = 'removeRowAnimation 0.5s';
                    rowElement.addEventListener('animationend', () => rowElement.remove());
                }
            });

        });
    }

}

class CheckBoxManager {
    constructor(table, tbody) {
        this.table = table;
        this.tbody = tbody;
        this.headerCheckbox = this.table.querySelector('thead input[type="checkbox"]');
    }

    initCheckedOrUnchecked() {
        this.checkedOrUncheckedAll();
        this.checkedOrUncheckedHeader();
    }

    checkedOrUncheckedAll() {
        this.headerCheckbox.addEventListener('change', () => {
            const checkboxes = this.getCheckboxList();
            checkboxes.forEach(checkbox => checkbox.checked = this.headerCheckbox.checked);
        });
    }

    checkedOrUncheckedHeader() {
        this.tbody.addEventListener('change', () => {
            const checkboxes = this.getCheckboxList();
            this.headerCheckbox.checked = Array.from(checkboxes).every(checkbox => checkbox.checked);
        });
    }

    getCheckboxList() {
        return this.table.querySelectorAll('tbody input[type="checkbox"]');
    }
}
