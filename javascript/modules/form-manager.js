export default class FormManager {
    constructor(formElement, openButton, closeButtons) {
        this.form = formElement;
        this.openButton = openButton;
        this.closeButtons = Array.isArray(closeButtons) ? closeButtons : [closeButtons];
        this.mainPageHeader = document.querySelector('.main-page__header');
    }

    init() {
        this.openButton.addEventListener('click', () => {
            this.displayForm();
        });

        this.closeButtons.forEach(button => {
            button.addEventListener('click', () => {
                this.closeForm();
            });
        });
    }

    displayForm() {
       this.mainPageHeader.style.zIndex = '0';
       this.form.parentElement.style.display = 'flex';
       this.form.style.display = 'inline-block';
    }

    closeForm() {
        this.form.parentElement.classList.add('section-form__unbluring');
        this.form.classList.add('form__animate-out');

        this.form.addEventListener('animationend', () => {
            this.form.style.display = 'none';
            this.form.classList.remove('form__animate-out');

            this.form.parentElement.style.display = 'none';
            this.form.parentElement.classList.remove('section-form__unbluring');

            this.mainPageHeader.style.zIndex = '1';
        }, { once: true });
    }
}
