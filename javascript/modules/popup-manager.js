export default class PopupManager {
    constructor() {
        this.popupContainer = document.getElementById('confirmationModal');
        this.popup = this.popupContainer.querySelector('.modal-content');
        this.confirmButton = this.popupContainer.querySelector('#confirmDelete');
        this.cancelButton = this.popupContainer.querySelector('#cancelDelete');
        this.mainPageHeader = document.querySelector('.main-page__header');
    }

    confirm() {
        this.popupContainer.style.display = 'block';
        this.mainPageHeader.style.zIndex = '0';

        // Повертаємо новий проміс
        return new Promise((resolve) => {
            this.confirmButton.onclick = () => {
                this.popupContainer.classList.add('modal--unbluring');
                this.popup.classList.add('modal-content--close');

                this.popupContainer.addEventListener('animationend', () => {
                    this.popupContainer.style.display = 'none';
                    this.mainPageHeader.style.zIndex = '1';

                    this.popupContainer.classList.remove('modal--unbluring');
                    this.popup.classList.remove('modal-content--close');
                }, { once: true });

                resolve(true);
            };

            this.cancelButton.onclick = () => {
                this.popupContainer.classList.add('modal--unbluring');
                this.popup.classList.add('modal-content--close');

                this.popupContainer.addEventListener('animationend', () => {
                    this.popupContainer.style.display = 'none';
                    this.mainPageHeader.style.zIndex = '1';

                    this.popupContainer.classList.remove('modal--unbluring');
                    this.popup.classList.remove('modal-content--close');
                }, { once: true });
                resolve(false);
            };
        });
    }
}