export default class TooltipManager {
    constructor(selector) {
        this.selector = selector;
        this.currentTooltip = null;
    }

    init() {
        document.addEventListener('click', (event) => this.handleDocumentClick(event));
    }

    handleDocumentClick(event) {
        const target= event.target.closest(this.selector);
        if (target) {
            const tooltip = target.parentNode.querySelector('.header__tooltip');
            this.toggleTooltip(tooltip);
        } else {
            this.hideCurrentTooltip();
        }
    }

    toggleTooltip(tooltip) {
        if (this.currentTooltip !== tooltip) {
            this.hideCurrentTooltip();
            this.applyTooltipStyle(tooltip, true);
        } else {
            this.hideCurrentTooltip();
        }
    }

    hideCurrentTooltip() {
        if (this.currentTooltip) {
            this.applyTooltipStyle(this.currentTooltip, false);
        }
    }

    applyTooltipStyle(tooltip, show) {
        if (tooltip) {
            tooltip.style.opacity = show ? '1' : '0';
            tooltip.style.visibility = show ? 'visible' : 'hidden';
            this.currentTooltip = show ? tooltip : null;
        }
    }
}
