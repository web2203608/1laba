import FormManager from './modules/form-manager.js';
import TableManager from './modules/student-table-manager.js';
import TooltipManager from './modules/tooltip-manager.js';
import Student from '../javascript/model/student.js';

const mainPageContent = document.querySelector('.main-page__content');
const studentData = new Student();

const mainPageContentObserver = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
        if (mutation.addedNodes.length) {
            const formAddStudent = mutation.target.querySelector('.section-form__form');
            const displayFormButton = mutation.target.querySelector('.student-list__display-form-button');
            const closeFormButton = mutation.target.querySelector("#close-button");
            const addStudentButton = mutation.target.querySelector("#add-button");

            if (formAddStudent && displayFormButton && closeFormButton && addStudentButton) {
                const formModule = new FormManager(formAddStudent, displayFormButton, [closeFormButton, addStudentButton]);
                formModule.init();
            }

            const studentTable = mutation.target.querySelector('.students-list__table');

            if (studentTable) {
                const tableModule = new TableManager(studentTable, addStudentButton, studentData);
                tableModule.init();
            }

            const tooltipManager = new TooltipManager('.header__icon--bell, .header__icon--user');
            tooltipManager.init();
        }
    });
});

mainPageContentObserver.observe(mainPageContent, { childList: true, subtree: true });