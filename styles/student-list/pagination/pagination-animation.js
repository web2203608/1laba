const mainPageContent = document.querySelector('.main-page__content');

const mainPageContentObserver = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
        if (mutation.addedNodes.length) {
            const btns = document.querySelectorAll('.btn');
            const paginationWrapper = document.querySelector('.pagination-wrapper');

            btns.forEach(btn => btn.addEventListener('click', btnClick));

            function btnClick() {
                // Ternary operator for classList manipulation
                this.classList.contains('btn--prev') ?
                    paginationWrapper.classList.add('transition-prev') :
                    paginationWrapper.classList.add('transition-next');

                setTimeout(cleanClasses, 500);
            }

            function cleanClasses() {
                paginationWrapper.classList.remove('transition-next', 'transition-prev');
            }
        }
    });
});

mainPageContentObserver.observe(mainPageContent, { childList: true, subtree: true });